package com.epam.bo;

import com.epam.po.EmailPage;

public class EmailBO {
  private EmailPage emailPage;

  public EmailBO() {
    emailPage = new EmailPage();
  }

  public EmailBO writeEmail() {
    emailPage
            .clickByWriteButton()
            .fillRecipientEmailInputField()
            .fillSubjectNameInputField()
            .fillTextAreaInputField()
            .clickByCloseButton();
    return this;
  }
}
