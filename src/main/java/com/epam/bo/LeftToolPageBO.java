package com.epam.bo;

import com.epam.po.LeftToolPage;

public class LeftToolPageBO {
  private LeftToolPage leftToolPage;

  public LeftToolPageBO() {
    leftToolPage = new LeftToolPage();
  }

  public LeftToolPageBO verifyMessageInDraftFolder() {
    leftToolPage
            .openLeftPage()
            .verifyIsMessageInDraftFolder();
    return this;
  }

  public LeftToolPageBO countEmailsBeforeSending(){
    leftToolPage.countNumberOfEmailsBeforeSending();
    return this;
  }
}
