package com.epam.utils;

import com.epam.constants.Constants;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.epam.constants.Constants.BASE_URL;

public abstract class DriverManager {

  private static AndroidDriver<WebElement> driver;

  protected void initDriver() throws MalformedURLException {
    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"Appium");
    desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"81c515a6");
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0.1");
    desiredCapabilities.setCapability("appPackage","com.google.android.gm");
    desiredCapabilities.setCapability("appActivity","com.google.android.gm.ConversationListActivityGmail");

    URL url = new URL(BASE_URL);

    driver = new AndroidDriver<WebElement>(url,desiredCapabilities);
    driver.manage().timeouts().implicitlyWait(Constants.IMPLICITY_WAIT_VALUE, TimeUnit.SECONDS);
  }

  public static AndroidDriver getDriver() {
    return driver;
  }

  protected void quitDriver() {
    driver.quit();
  }
}
