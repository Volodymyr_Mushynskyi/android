package com.epam.po;

import com.github.javafaker.Faker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.epam.utils.DriverManager.getDriver;

public class EmailPage extends BasePage {

  private static final Logger logger = LogManager.getLogger(EmailPage.class);
  private WebDriverWait wait = new WebDriverWait(getDriver(), 15);
  private Faker fake = new Faker();

  @FindBy(id = "com.google.android.gm:id/compose_button")
  private WebElement writeButton;

  @FindBy(id = "com.google.android.gm:id/to")
  private WebElement recipientEmailInputField;

  @FindBy(id = "com.google.android.gm:id/subject")
  private WebElement subjectNameInputField;

  @FindBy(id = "com.google.android.gm:id/composearea_tap_trap_bottom")
  private WebElement textAreaInputField;

  @FindBy(className = "android.widget.ImageButton")
  private WebElement closeButton;

  public EmailPage() {

  }

  public EmailPage clickByWriteButton() {
    logger.info("Click by write button");
    writeButton.click();
    return this;
  }

  public EmailPage clickByCloseButton() {
    logger.info("Click by send button");
    closeButton.click();
    return this;
  }

  public EmailPage fillRecipientEmailInputField() {
    logger.info("Fill recipient address");
    recipientEmailInputField.sendKeys("volodya2127@gmail.com");
    return this;
  }

  public EmailPage fillSubjectNameInputField() {
    logger.info("Fill subject field");
    subjectNameInputField.sendKeys(fake.internet().domainName());
    return this;
  }

  public EmailPage fillTextAreaInputField() {
    logger.info("Fill text area");
    textAreaInputField.click();
    textAreaInputField.sendKeys(fake.internet().macAddress());
    return this;
  }
}
