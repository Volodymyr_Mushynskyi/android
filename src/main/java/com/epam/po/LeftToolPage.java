package com.epam.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static com.epam.utils.DriverManager.getDriver;


public class LeftToolPage extends BasePage {

  private static final Logger logger = LogManager.getLogger(LeftToolPage.class);
  private String numberOfEmails;

  public LeftToolPage() {

  }

  public LeftToolPage verifyIsMessageInDraftFolder() {
    WebDriverWait wait = new WebDriverWait(getDriver(), 10);
    int numberOne;
    int numberTwo = Integer.valueOf(numberOfEmails);
    logger.info("Verify is message in draft folder");
    numberOne = Integer.valueOf(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
            "//android.widget.LinearLayout[contains(@index,'8')]//(android.widget.TextView)[2]"))).getText());
    Assert.assertEquals(compareDigits(numberTwo, numberOne), 1);
    return this;
  }

  private int compareDigits(Integer numberOne, Integer numberTwo) {
    if (numberTwo > numberOne) {
      return 1;
    }
    if (numberTwo < numberOne) {
      return -1;
    }
    return 0;
  }

  public void countNumberOfEmailsBeforeSending() {
    WebDriverWait wait = new WebDriverWait(getDriver(), 10);
    numberOfEmails = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
            "//android.widget.LinearLayout[contains(@index,'8')]//(android.widget.TextView)[2]"))).getText();
  }

  public LeftToolPage openLeftPage() {
    openLeftToolPage();
    return this;
  }
}
