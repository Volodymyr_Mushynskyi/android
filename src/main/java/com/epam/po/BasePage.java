package com.epam.po;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.DriverManager.getDriver;

public class BasePage {

  public BasePage() {
    PageFactory.initElements(getDriver(),this);
  }

  public void openLeftToolPage(){
    WebDriverWait wait = new WebDriverWait(getDriver(), 5);
    wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
  }
}
