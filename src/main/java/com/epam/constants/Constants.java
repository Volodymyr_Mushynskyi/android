package com.epam.constants;

public class Constants {
  public static final int IMPLICITY_WAIT_VALUE = 10;
  public static final String BASE_URL = "http://127.0.0.1:4723/wd/hub";

  private Constants() {
  }
}
