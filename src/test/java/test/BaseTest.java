package test;

import com.epam.constants.Constants;
import com.epam.utils.DriverManager;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

public class BaseTest extends DriverManager {
  private final Logger logger = LogManager.getLogger(BaseTest.class);
  private Test test;

  @BeforeMethod(alwaysRun = true)
  public void beforeMethod(Method method) throws MalformedURLException {
    initDriver();
    BasicConfigurator.configure();
    test = method.getAnnotation(Test.class);
    logger.info(String.format("Test '%s' started.", method.getName()));
    logger.info(String.format("Description: '%s'", test.description()));
  }

  @AfterMethod(alwaysRun = true)
  public void afterMethod(final Method method) {
    logger.info(String.format("Test '%s' completed.", method.getName()));
    quitDriver();
  }
}
