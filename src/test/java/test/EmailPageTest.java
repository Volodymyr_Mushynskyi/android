package test;

import com.epam.bo.EmailBO;
import com.epam.bo.LeftToolPageBO;
import org.testng.annotations.Test;

public class EmailPageTest extends BaseTest {

  @Test
  public void sendingEmail() {

    new LeftToolPageBO()
            .countEmailsBeforeSending();
    new EmailBO()
            .writeEmail();
    new LeftToolPageBO()
            .verifyMessageInDraftFolder();
  }
}
